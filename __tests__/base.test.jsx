import React from "react";
import {configure, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Base from "../views/base.jsx";

configure({adapter: new Adapter()});

describe("<Base />", () => {

    it("should render", () => {
        const wrapper = mount(<Base/>);
        expect(wrapper).toMatchSnapshot();
    });

});
