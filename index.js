"use strict";

const path = require("path");
const express = require("express");
const fs = require("fs");
const app = express();

require("node-jsx-babel").install();

// expose public folder as static assets
app.use("/public", express.static(__dirname + "/public"));

app.get("/*", function (req, res) {

    res.writeHead(200, {"Content-Type": "text/html"});

    fs.readFile(__dirname + "/views/index.html", "utf-8", function (err, content) {

        if (err) {
            res.end("error occurred");
            return;
        }

        let config = {
            FILE_HOST: process.env.FILE_HOST,
            NODE_ENV: process.env.NODE_ENV,
            SERVICE_HOST: process.env.SERVICE_HOST
        };

        let _content = content.replace("{{config}}", JSON.stringify(config));

        res.end(_content);

    });

});

const server = app.listen(3000, function () {

    let host = server.address().address;
    let port = server.address().port;

    console.log("Bar app listening at http://%s:%s", host, port);

});

module.exports = app;