"use strict";

import React from "react";
import {Route, Router} from "react-router";

import Base from "./base.jsx";

let routes = (props) => {
    return (
        <Router  {...props}>
            <Route path="/" component={Base}/>
        </Router>
    );
};

module.exports = routes;