import React from "react";
import RestClient from "node-rest-client";

import Button from "../components/Button/Button.jsx";
import ProgressBar from "../components/ProgressBar/ProgressBar.jsx";

class view extends React.Component {

    constructor() {

        super();

        this.state = {
            buttons: [], // array of buttons
            bars: [], // array of bars
            limit: 100, // default
            selectedProgressBar: 0
        };

    }

    componentDidMount() {

        let _self = this;

        // pull the data from the end point
        let client = new RestClient.Client();
        client.get("http://pb-api.herokuapp.com/bars", (data, res) => {
            if (res.statusCode === 200) {
                _self.setState({buttons: data.buttons, bars: data.bars, limit: data.limit});
            } else {
                console.error("I fink da endpoint iz broke, cuz");
            }
        });

    }

    render() {

        return (
            <div className="view">

                <h1>Progress Bar Demo</h1>

                <div className="ProgressBars">


                    {this.state.bars.map((item, key) => {

                        return (
                            <ProgressBar key={key}
                                         value={Math.round(this.calcPerc(item))}
                                         isSelected={(key === this.state.selectedProgressBar)}
                                         onClick={this.selectProgressBar.bind(this, key)}/>
                        );
                    })}
                </div>
                <div className="Buttons">
                    {this.state.buttons.map((item, key) => {
                        return (
                            <Button key={key} value={item}
                                    onClick={this.updateProgressBar.bind(this)}>{item + "%"}</Button>
                        );
                    })}
                </div>
            </div>
        );

    }

    calcPerc(value) {
        return (value / this.state.limit) * 100;
    }

    calcValue(perc) {
        return (this.state.limit / 100) * perc;
    }

    selectProgressBar(key) {
        this.setState({selectedProgressBar: key});
    }

    updateProgressBar(perc) {

        let bars = this.state.bars;

        let value = this.calcValue(perc);
        let newValue = this.calcValue(perc) + bars[this.state.selectedProgressBar];

        bars[this.state.selectedProgressBar] = (newValue < 0) ? 0 : newValue;

        this.setState({bars: bars});

    }

}

export default view;