"use strict";

import React from "react";
import ReactDOM from "react-dom";
import Base from "./base.jsx";

// require("./views/**/*.jsx", {glob: true});

ReactDOM.render(
    <Base/>,
    document.getElementById("root"));