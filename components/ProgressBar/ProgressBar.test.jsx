import React from "react";
import {configure, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ProgressBar from "./ProgressBar";

configure({adapter: new Adapter()});

describe("<ProgressBar />", () => {

    it("should render", () => {
        const wrapper = mount(<ProgressBar/>);
        expect(wrapper).toMatchSnapshot();
    });

    it("should fire click handler on click event", () => {

        const clickHandler = () => {
            expect(true).toBe(true);
        };

        const wrapper = mount(<ProgressBar onClick={clickHandler}/>);

        wrapper.find("div.wrapper").simulate("click");

    });

    it("should have the 'selected' class if it's selected", () => {

        const wrapper = mount(<ProgressBar isSelected={true}/>);

        expect(wrapper.find(".ProgressBar").hasClass("selected")).toEqual(true);

    });

    it("should not have the 'selected' class if it's props.isSelected === false", () => {

        const wrapper = mount(<ProgressBar/>);

        expect(wrapper.find(".ProgressBar").hasClass("selected")).toEqual(false);

    });

    it("should have the 'exceeded' class if props.value gt 100", () => {

        const wrapper = mount(<ProgressBar value={110}/>);

        expect(wrapper.find(".ProgressBar").hasClass("exceeded")).toEqual(true);

    });

    it("should not have the 'exceeded' class if props.value lte 100", () => {

        const wrapper = mount(<ProgressBar value={100}/>);

        expect(wrapper.find(".ProgressBar").hasClass("exceeded")).toEqual(false);

    });

});
