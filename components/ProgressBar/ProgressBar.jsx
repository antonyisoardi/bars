import React from "react";

class ProgressBar extends React.Component {

    constructor() {

        super();

        this.state = {};

    }

    render() {

        let classes = "ProgressBar";
        if (this.props.isSelected) classes += " selected";
        if (this.props.value > 100) classes += " exceeded";

        let width = ((this.props.value > 100) ? 100 : this.props.value);

        return (
            <div className={classes}>
                <div className="wrapper" onClick={this.handleClick.bind(this)}>
                    <div className="value">{this.props.value + "%"}</div>
                    <div className="fill" style={{width: width + "%"}}/>
                </div>
            </div>
        );
    }

    handleClick() {

        if (this.props.onClick) this.props.onClick();

    }

}

export default ProgressBar;