import React from "react";

class Button extends React.Component {

    constructor() {
        super();

    }

    render() {

        return (
            <a className="Button" href="#" onClick={this.handleClick.bind(this)}>{this.props.children}</a>
        );

    }

    handleClick() {

        if (this.props.onClick) this.props.onClick(this.props.value);

    }

}

export default Button;