import React from "react";
import {configure, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Button from "./Button.jsx";

configure({adapter: new Adapter()});

describe("<Button />", () => {

    it("should render", () => {
        const wrapper = mount(<Button/>);
        expect(wrapper).toMatchSnapshot();
    });

    it("should fire click handler on click event", () => {

        let value = 1;

        const clickHandler = (_value) => {
            expect(_value).toBe(value);
        };

        const wrapper = mount(<Button value={value} onClick={clickHandler}/>);

        wrapper.find("a.Button").simulate("click");

    });

});
