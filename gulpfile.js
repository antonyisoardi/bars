const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const inject = require("gulp-inject");

// Concat Components SCSS
gulp.task("concat-components", function () {

    gulp.src("./scss/components.scss")
        .pipe(inject(gulp.src("./components/**/**/*.scss", {read: false}), {relative: true}))
        .pipe(gulp.dest("./scss/"));

});

// Compile CSS
gulp.task("sass", function () {
    // compile css for foundation 6
    return gulp.src("./scss/main.scss")
        .pipe(sass({outputStyle: "compressed"}))
        .pipe(gulp.dest("./public/css/"));
});

gulp.task("build", ["concat-components", "sass"]);